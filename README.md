# Iniciativa kubernetes
AULA 1 – CONTAINERS E DOCKER SIMPLIFICADOS



<h1 align='center'>Instruções:</h1>

<ol>
    <li>
    Baixe a imagem no docker hub<br/> docker pull ircarvalho/conversor
    </li>
    <li>Execute o comando <br/>
        <b>docker run --rm  -tid -p 8080:8080 ircarvalho/conversor</b>
    </li>
    <li> Acesse a Imagem no dockerhub
        https://hub.docker.com/r/ircarvalho/conversor
    </li>
    <li>Repositório <br/>
        https://gitlab.com/iniciativakubernets
    </li>
    <li>Aplicação<br/>
        http://localhost:8080'
    </li>
</ol>
